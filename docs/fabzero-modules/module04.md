# 4. La découpeuse laser

Ce module consiste en l'apprentissage d'un outil du Fablab qui pourrait nous servir pour notre projet de fin d'année. N'ayant pas su participer à une des premières formations, [Antoine Trillet](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/antoine.trillet/) a accepté de me montrer comment fonctionnait *l'Epilog Fusion Po 32* et m'a suivi durant mon processus pour mon modèle.

![Antoine](images/module4/antoine.jpg)

Contrairement à l'imprimante 3D, cet appareil demande très peu de programmes si ce n'est qu'un logiciel qui permet la réalisation d'une image **vectorielle** et qui utilise donc notamment des formats SVG (Scalable Vector Graphics). J'ai décidé de travailler avec celui proposé par le cours : [InkScape](https://inkscape.org/)

Il y a 3 machines disponibles au FabLab : *L'Epilog Fusion Pro 32*, *Lasersaur* et la *Full Spectrum Muse*

Les matériaux les plus souvent utilisés pour des découpes sont :


- Du carton ou du papier
- Du bois
- Des textiles 
- Du Plexiglass/PMMA


En revanche, d'autres matériaux sont à ne **surtout pas** utiliser :


- PVC et Téflon : produit de la fumée acide et nocive
- Cuivre : réfléchit totalement le laser (même si j'ai une folle envie de voir comment ça se passe)
- Résine phénolique, epoxy : produit de la fumée uniquement nocive
- Vinyl, simili-cuir
- Cuir animal : met de la poussière partout et l'odeur persiste



Toutes les machines ne découpent pas de la même façon : certains matériaux seront alors moins évidentes à utiliser. Il faut aussi noter rque pour **découper** certains matériaux plus épais, il faudra très probablement repasser plusieurs fois dessus pour avoir le résultat souhaité.


## Gravures et découpes 

Les machines à notre disposition peuvent réaliser des *découpes* ou des *gravures* : 



- **La découpe vectorielle** : utilisée pour des découpes précises et rapides, un fichier .svg est nécessaire

- **La gravue vectorielle** : fonctionne de la même façon que pour la découpe, la puissance du rayon est cependant moins forte

- **La gravure matricielle** : utilisée pour réaliser une image à la surface du matériel. La profondeur de la gravure sera proportionnelle à la luminosité sur le dessin : les parties sombres seront plus profondes. 




L'utilisation de *couleurs* différentes lors de la modélisation peut-être intéressante si il faut des découpes/gravures de paramètres différents.
## Mesures de sécurité


- Sans surprise, bien connaître le matériel qui va être utilisé

- Activer **l'air comprimé** - sauf pour l'Epilog, elle le fait automatiquement

- Allumer **l'extracteur de fumée**

- Localiser le bouton d'arrêt d'urgence

- Avoir un extincteur au CO2 pas loin, dans le doute


- Ne pas regarder fixement le laser (testé et déconseillé)

- Ne pas aller boire un café pendant la découpe

- Mettre les lunettes de protection validées par Karl Lagerfeld



## Lasersaur

Contrairement aux autres découpeuses disponibles au FabLab, la Lasersaur, machine à laser CO2 (infrarouge), est la moins évidente à utiliser. Ses dimensions de découpe maximale sont de **120x60cm** et peut aller jusque **12cm** de profondeur. Le logiciel à ouvrir sur l'ordinateur déjà mis en place s'appelle *Driveboard app*. Elle ne lit que les **formats .svg** et étrangement, celle du FabLab n'arrive pas à lire les modèles depuis *Illustrator* : il faut passer par *InkScape*. Les gravures de type photo ne sont pas réalisables. Un [manuel](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Lasersaur.md) est disponible sur Gitlab.

Avant de lancer la découpe d'un modèle, il faut vérifier la hauteur du laser avec un outil disponible : il est recommandé d'utiliser le 13mm.


![](images/module4/saurtruc.jpg)
![](images/module4/saurmesure.jpg)


Après avoir ouvert le fichier sur l'ordinateur de l'appareil, la découpe peut être lancée si tous les statuts apparaissent en vert sur le programme. Le bouton `Run` permet de contourner la zone du matériel qui va être utilisée. 

![](images/module4/saurrun.jpg)

Au niveau de la sécurité, la Lasersaur se compose de 2 extracteurs, 1 refroidisseur et un tuyau d'air comprimé : il faut veiller à ce que tout soit allumé et que le tuyau soit ouvert. Le refroidisseur doit être évidemment activé, il doit aussi apparaître en vert.

![](images/module4/saurextr.jpg)
![](images/module4/saurair.jpg)


## Full Spectrum Muse

La Full Spectrum Muse peut faire des découpes de taille maximale **A2** (50x30cm) avec **6cm de hauteur** et le matériel recommandé est le **carton**. Elle utilise un laser de type CO2 (infrarouge) ainsi qu'un pointeur rouge. Si elle fait partie d'une des machines les plus simples à utiliser, c'est parce qu'elle possède son **propre réseau wifi**, ce qui permet une connexion directe avec mon ordinateur et un accès facile à *Retina Engrave*. Après s'être connecté, il faut copier l'adresse affichée dans un navigateur, ce qui m'amène directement aux paramètres de découpe. La machine importe automatiquement le fichier comme **gravure** : si il faut une découpe, le fichier doit être supprimé.

![](images/module4/musewifi.png)



## Epilog Fusion Pro 32
![epilog](images/module4/epilog.jpg)


La 3e découpeuse disponible au FabLab est l'Epilog Fusion Pro 32, qui fonctionne au laser CO2 (infrarouge). Cet appareil assez performant permet de faire des **découpes** comme des **gravures**. C'est avec celle-ci que j'ai travaillé pour créer mon modèle 3D en carton (littéralement). Ses dimensions maximales sont de 81 x 50 cm et 31cm en hauteur. Voici son [manuel](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Epilog.md) d'utilisation.


### Réalisation & impression d'un modèle 3D

Comme première tentative à la découpe laser, j'ai voulu réaliser sur *InkScape* une sorte de pyramide à base triangulaire avec des ailerons sur le côté pour pouvoir les coller à l'intérieur et structurer le tout. J'ai besoin de 2 découpes différentes : une pour découper tout le contour de ma pièce et une moins profonde pour réaliser les plis à faire. 
Le début m'a pris un peu de temps avant de comprendre l'interface du logiciel.

Pour pouvoir réaliser les triangles, j'ai sélectionné l'option *polygon* et modifié le nombre d'angles. Les traits ont ensuite été réalisés avec l'option *Bezier curves and straight lines*.

![polygon](images/module4/polygon.png)
![lines](images/module4/lines.png)


Les paramètres comme le remplissage, l'épaisseur ou la couleur des lignes et formes peuvent être sélectionnés dans la fenêtre de droite : 

![fill](images/module4/fill.png)
![stroke](images/module4/stroke.png)
![stroke style](images/module4/strokestyle.png)


Puisqu'il me faut 2 types de découpe différentes, j'ai sélectionné 2 couleurs qui seront facilement distinguées par la machine : les traits noirs doivent être découpés entièrement pendant que les rouges ne seront que gravés. Après avoir réalisé le modèle souhaité, il faut sauvegarder son fichier en **.svg**.  

![dessin inkscape](images/module4/triforcekirigami.png)


J'upload ensuite le fichier .svg sur une clé USB qu'il faudra insérer dans l'ordinateur relié à la machine [^1]. J'ai décidé d'utiliser l'*Epilog Fusion Pro* par question de facilité. 
Pour cette découpe, j'ai choisi un morceau de carton le moins épais disponible ; le matériau peut être directement inséré dedans, mais il faut veiller à ce qu'il reste bien plat sur la grille (et pas ondulé). Sur l'ordinateur en question, il faut utiliser le programme *Epilog Dashboard* (s'il n'est pas déjà ouvert). L'epilog Fusion Pro 32 comporte une caméra sur le coté intérieur de l'ouverture, qui permet sélectionner l'emplacement de la découpe par rapport au matériau inséré.


![caméradécoupe](images/module4/camera.jpg)



Il faut ensuite cliquer sur impression, en sélectionnant la bonne machine. 



![impressionmachine](images/module4/imprimer.jpg)


Une série de paramètres apparaît par la suite à droite de l'écran, en deux tabs différentes pour ma part. L'imprimante a ici détecté 2 couleurs bien distinctes dans le modèle, ce qui me permet de choisir des paramètres différents pour chacune d'entre-elles.
Ces paramètres sont à définir en fonction du **matériau utilisé** et de la **découpe/gravure désirée** via les *grilles de calibration* placées à côté :

![planches](images/module4/epplanchespost.jpg)


- Vitesse 
- Puissance
- Fréquence (elle peut-être laissée à 100%)



Comme je l'ai mentionné plus haut, mes lignes *rouges* sont mes lignes de pli et demandent donc une découpe plus légère (qu'on peut qualifier de gravure); à l'inverse, mes lignes *noires* doivent être découpées entièrement. J'ai donc sélectionné une **puissance plus élevée** pour mes lignes noires et **moins élevées** pour mes lignes rouges. Le carton sélectionné étant relativement fin (comparé à du bois), j'ai laissé le **nombre de passes** en 1.


### Essai #1 

|           | Vitesse | Puissance | 
|----------|----------|-----------|
|Lignes noires| 50.0% | 70.0% | 
|Lignes rouges| 50.0% | 10.0% |


![essai1](images/module4/laseressai1.jpg)


L'impression a pris moins d'une minute : le résultat obtenu était assez précis, mais les découpes n'étaient pas du tout assez profondes dans les deux cas. 


### Essai #2

J'ai donc décidé d'augmenter la puissance des lignes rouges et de diminuer la vitesse des lignes noires.
En plus de cela, j'ai sélectionné l'option "palpeur" dans "Autofocus", qui permet à l'appareil de calculer l'épaisseur de mon matériau elle-même.


|           | Vitesse | Puissance | 
|----------|----------|-----------|
|Lignes noires| 5.0% | 70.0% | 
|Lignes rouges| 50.0% | 30.0% |



![essai2](images/module4/IMG_8003.jpg)
![essai2](images/module4/IMG_8004.jpg)



Pendant l'impression (qui a duré plus longtemps que la première), j'ai vite compris que j'y suis peut-être allée un peu trop fort pour la puissance des lignes noires (ou choisi une vitesse trop faible) :

![casentlebrule](images/module4/IMG_8001.jpg)


Sans surprise, les lignes noires étaient non seulement bien découpées mais surtout bien brûlées sur le contour : les résidus noirs et l'odeur de cramé n'ont pas enchanté Antoine. En revanche, mes lignes rouges étaient d'une profondeur presque parfaite (j'aurais pu y aller un rien plus fort); j'ai su plier mes bords plus ou moins correctement. Mes ailerons étaient mal modélisés donc elles rentraient moins bien dans le triangle : il aurait fallu les faire en forme de trapèze. 


![impression2](images/module4/IMG_8005.jpg)
![résultat](images/module4/IMG_8006.jpg)


![résultat](images/module4/IMG_8007.jpg)


Au final, j'ai tout de même réussi à coller les ailerons à l'intérieur de mes triangles pour obtenir le modèle souhaité.


![final](images/module4/final.jpg)






[^1]: Si comme moi, vous avez un macbook avec que des entrées USBC, pensez à prendre votre adaptateur avec (ou un ami qui accepte gentiment de faire le transfert sur son pc).

