# 3. Impression 3D

Lors de cette séance, j'ai appris les bases du fonctionnement d'une imprimante 3D, comment modéliser un objet en 3D et comment l'imprimer par après en prenant en compte tous les paramètres proposés par la machine.

## Différents types d'imprimantes 3D

Il existe différents types d'imprimantes 3D : certaines fonctionnent avec un lit de poudre (meilleure précision, souvent utilisée pour produire des pièces en métal), d'autres par bio-impression 3D (dépot de matériaux biologiques, utilisée principalement en bio-ingénierie et en cuisine de précision), par stéréolithographie, etc. 

Les imprimantes à disposition au FabLab sont des imprimantes 3D *FDM (fused-deposition modeling) Prusa* qui fonctionnent par dépôt de fondu : des bobines de filaments thermoplastiques sont fondues puis resolidifés quand ils sont déposés sur la plaque. Ces mécanismes de fonte et de solidification sont réalisées à des vitesses assez élevées, pour permettre la création de "couches" qui vont après un loooong temps d'attente, former l'objet souhaité. Les imprimantes au FabLab impriment généralement avec du PLA. Il est également intéressant de noter que la majorité des composants de ces imprimantes ont eux-mêmes été réalisés à l'imprimante 3D : autrement nommé *"Prusaception"*.

Pour autant que les impressimantes 3D soient terriblement pratiques et intéressantes, elles possèdent leurs propres limites. Au niveau de l'impression en tant que telle, lorsque des parties d'un objet sont en suspension, il faut penser à mettre des supports. Ceux ci doivent être bien réglés pour ne pas passer 2 heures à les retirer une fois la pièce réalisée (de nouveau, testé et non approuvé). L'imprimante elle même est limitée au niveau des dimensions d'impressions : les Prusa disponibles au FabLab ne peuvent pas dépasser les impressions de plus de 21x21cm. 

## PrusaSlicer

Après avoir modélisé mon objet, je dois le convertir (le modèle) en un ensemble d'instructions que l'imprimante 3D pourra suivre pour le créer. Les imprimantes du FabLab étant des *Prusa*, j'ai du installer le logiciel [**PrusaSlicer**](https://help.prusa3d.com/article/install-prusaslicer_1903) comme *Slicer[^1]*.
Le logiciel permet la configuration de plusieurs paramètres concernant l'impression :


- La densité de remplissage
- L'épaisseur de la couche 
- La vitesse d'impression 
- La température 
- L'orientation de l'objet sur le plateau 


Ces paramètres vont me permettre d'imprimer l'objet avec une meilleure précision et qualité.

Après avoir sauvegardé mon modèle depuis OpenSCAD en .stl, je l'ai ouvert dans PrusaSlicer. J'ai ensuite sélectionné le modèle de l'imprimante qui allait être utilisé et j'ai défini les paramètres qu'il me fallait pour réaliser ma pièce. N'ayant pas tout compris directement, je n'ai pas fait de grandes modifications aux paramètres de base. J'ai par la suite appris qu'il y avait un [guide](https://help.prusa3d.com/materials#_ga=2.244086342.219393710.1647608806-1375085091.1647608806) pour les paramètres, à définir en fonction du matériau utilisé.


En ayant exporté le fichier en .stl, j'obtiens :

![En ayant exporté le fichier en .stl, j'ai](images/module3/Modelestl.png)
![stl modele2](images/module3/m2stl.png)

En **important** ce fichier dans *Prusa Slicer* et après avoir sélectionné mes paramètres, le logiciel m'affiche ma pièce avec les détails des couches :
![J'obtiens donc ceci en ouvrant le fichier .stl après avoir sélectionné mes paramètres](images/module3/modeleslicer.png)
![m2 prusa](images/module3/m2prusa.png)


Finalement, j'exporte le G-code[^2] de ma pièce et je l'ai upload sur une carte SD que je vais insérer dans l'imprimante. 

### Pièce #1

Après un peu plus de 6 minutes d'impression, le résultat ne m'a pas entièrement convenu :

![oups](images/module3/modelereel.jpg)

Effectivement, je n'avais pas pensé au fait que l'angle de 90° sur le coté supérieur allait poser problème au niveau de l'impression. Ma barrre horizontale finale n'avait pas de support en dessous pour tenir, donc le plastique a fini dans le vide. Pour régler ce souci, j'aurais du faire un angle courbé mais saccadé tous les x degrés. Pour ce faire, j'aurais scripté un cylindre creux de l'épaisseur de la tige verticalle, avec le code `$fn` pour choisir le nombre d'angles dans ce cylindre. Plus il y a d'angles dans une courbure, plus l'impression a une chance d'être réalisée correctement (sans pour autant être circulaire).
 Je reste par contre satisfaite du reste de la pièce concernant l'épaisseur et la densité des couches. 


 ### Pièce #3 

 L'impression ici a duré 22 minutes, mais le résultat état nettement meilleur que ma pièce précédente. Contrairement à elle, ma pièce #2 n'a pas eu besoin de supports car rien n'était en suspension ; ma pièce est ressortie parfaitement coomme je le voulais. 

 ![noice](images/module3/noice.jpg)

 Mais évidemment, si tout s'est bien passé pendant mon impression, c'est parce qu'un problème résidait ailleurs... 

 ![piecemartin](images/module3/piecemartin.jpg)

 Et oui, la pièce de Martin faisait à peu près 3 fois la talle de la mienne en terme de proportions. Autant vous dire que ça m'a mis de mauvaise humeur.

 En plus de ce petit souci de proportions, la tige réalisée comme partie flexible était beaucoup trop épaisse et pas assez longue à mon gout. Le risque de la peter est bien présente.




[^1]: Un Slicer est un logiciel qui va découper le modèle créé en tranches pour que l'imprimante puisse réaliser son travail.
[^2]: Langage utilisé dans la programmation : permet de contrôler des machines CNC