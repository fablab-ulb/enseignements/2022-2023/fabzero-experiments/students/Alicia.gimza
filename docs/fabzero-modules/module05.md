# 5. Dynamique de groupe et projet final

Lors de cette session, les nous avons appris à analyser une problématique et trouver des solutions respectives. Il s'agit d'un exercice assez important et intéressant puisque nous allons avoir un projet à réaliser pour la fin du quadrimestre, chacun avec notre groupe de projet.

## 5.1. Project Analysis & Design 

### Décortiquer une problématique : la méthode des arbres à problèmes & à solutions

*L'arbre à problèmes* est une méthode assez instinctive et simple qui permet de disperser tout ce que l'on sait à propos de notre problématique, et plus précisément au niveau des causes et des conséquences. Je commence par dessiner un **tronc** qui représente la problématique choisie, suivi des **racines** qui représentent les causes et les **branches** qui représentent les conséquences de celle-ci.

*L'arbre à solutions*, en revanche, consiste à placer l'objectif désiré en fonction de la problématique choisie au sein du tronc. Les racines caractérisent ce qui permettrait cette solution, et les branches des conséquences à la solution. Ce qui est intéressant avec ces arbres, c'est de voir que l'on peut trouver une solution ou apporter quelque chose sur une problématique autant au niveau des conséquences que des causes. On *spread* alors énormément le champ des possibilités.

### Le tourisme marin : arbre à problèmes

Pour cet exercice, j'ai choisi comme problématique le *tourisme marin* et j'en ai fait un arbre à problèmes. 

![arbre problèmes](images/module5/arbreprobleme.png)

Suite à ça, je peux choisir un objectif assez évident par rapport à ma problématique : *minimiser les conséquences négatives du tourisme marin* et de là, en créer un arbre à solutions.


### Minimiser les conséquences négatives du tourisme marin : arbre à solutions

En réfléchissant un peu aux causes et conséquences évoquées dans mon arbre à problèmes, j'ai pu identifier respectivement les répercussions positives de cette solution et ce qui la permettrait en se concentrant sur les causes de la problématique.

![arbre solutions](images/module5/arbresolution.png)


## 5.2. Group formation & brainstorming on project problems

Lors de cette séance, on nous a demandé d'arriver avec un objet qui représentait une problématique qui nous intéressait. J'ai décidé de préndre un poussin en plastique

Regardez comme il est mignon


![poussin](images/module5/poussin.jpg)



Ma problématique choisie était **la cause animale**. 

### 5.2.1. Création du groupe

Avec la guidance de Chloé, notre sociologue de référence, nous avons suivi plusieurs étapes afin de créer, sans le savoir, notre futur groupe de projet.

1. En arrivant au cours, en cercle, chacun d'entre nous a montré son objet sans évoquer la problématique choisie
2. Après, nous nous sommes tous un peu mélangés pour voir ce qu'étaient les problématiques de chacun
3. Finalement, nous avons du retrouver des personnes qui avaient des objets (et pas les **problématiques** !) qui présentaient un lien entre eux. Des formes de 5 ont été crééés. 
Toutes ces étapes ont mené à la création de la **perfection** : notre groupe de projet, AKA *Fabulous 7*.

J'ai trouvé ce processus (passer par un objet) assez intéressant et efficace, puisque, sans savoir ce que les autres avaient choisi comme problématique, je me suis retrouvée dans un groupe où l'on partageait toutes et tous un grand intérêt commun : la protection du monde marin.
Cet intérêt nous a donc permis d'identifier des problématiques sur lesquelles on pouvait axer notre projet assez facilement.

### Liste de problématiques possibles 

- La pollution liée au transport maritime
- Les marées noires
- La dépendance pétrolière
- La perte de la diversité dans l'océan
- La sur-pêche
- Le tourisme marin
- Les déchets en mer
- Les microplastiques

Vu notre enthousiasme, nous avons eu dès cette séance énormément d'idées qui se sont propagées et enflammées : parfois un peu trop, nous avions eu du mal à sélectionner une problématique alors que l'on avait déjà trouvé une solution pour d'autres. 


## 5.3. Dynamique de groupe
Cette séance, animée elle aussi par Pauline, nous a surtout appris à améliorer nos futures réunions avec notre groupe de projet. Cette approche au cours était donc beaucoup plus psychologique et axée sur les possibles fonctionnements de chacun. Elle a évoqué un grand nombre d'options et de méthodes qui rendraient nos réunions plus efficaces et optimales, notamment via :

- Comment introduire/préparer une réunion
    
- Le rôle de chacun
    Distribuer des rôles (secrétaire, animateur, gestionnaire de temps,...) lors des réunions permet de se diviser le travail facilement entre chacun et assure une certaine polyvalence. Cette option n'est cependant pas toujours efficace puisque chaque personne fonctionne différemment : certains groupes vont parfois mieux fonctionner lorsque les rôles attribués sont conservés tout le long.
- La communication
    Tout le monde n'ayant pas les mêmes capacités ou facilités à communiquer, ou pour tout simplement éviter de devoir se répéter, c'est  très utile de choisir des techniques assez simples pour montrer son accord ou désaccord (parfois gestuellement).
- La prise de décision
    Devoir prendre une décision est de mon point de vue un des points les plus chronophages, surtout pour des projets. Les situations les plus complexes sont souvent celles où les appréciations (ou non) de chacun sont très ressemblantes ou lorsqu'il y a des arguments pour et contre en même quantité pour chaque option.

Tous ces points cités permettraient, en théorie, un gain de temps et une bonne dynamique au sein du groupe, ce qui provoquerait sans doute une bonne efficacité au sein du projet. 

Au niveau de notre groupe, nous avons choisi 3 méthodes pour améliorer notre dynamique de groupe et optimiser nos réunions/séances : 
g


- La **météo d'entrée** : cela nous semblait important et intéressant de préciser notre état mental au début de chaque séance. Cela aiderait à mieux cibler qui est plus apte à faire tel ou tel travail, ainsi que les rôles.

    *Plus concrètement, la météo d'entrée consiste à expliquer en 2-3 mots comment l'on se sent à ce moment là*

- Un **ordre du jour** : un ODJ instauré et planifié avant la réunion permet de structurer les idées et savoir au préalable sur quoi il va falloir se concentrer et réfléchir. C'est aussi, d'après moi, une bonne façon de voir l'évolution du projet et du groupe.

    *L'ordre du jour est une petite liste qui contient les points qui seront/ doivent être abordés lors de la réunion et l'ordre.*

- La **température des mains** et les **votes pondérés** semblent être les techniques les plus efficaces en terme de prises de décision. La température des mains permet de communiquer son accord ou non avec l'idée d'une personne sans pour autant prendre la parole et couper la personne. Les votes pondérés sont assez efficaces lorsqu'il faut prendre la décision à prendre provoque une grosse hésitation parce que tout le monde est d'accord (ou non) avec les options : la subtilité de l'appréciation est mise en avant et démontrée via un nombre de croix.

    *La température des mains se réalise en secouant sa main en haut, en bas ou au centre pour montre son degré d'accord/de motivation.*
    
    *Les votes pondérés sont fait comme suit : face à chaque option, chaque personne possède 3 croix qui traduisent l'intensité de l'appréciation. Le nombre de croix appliqués par une personne ne peut être répété : chaque option a un nombre de croix différent (pour une personne)*


