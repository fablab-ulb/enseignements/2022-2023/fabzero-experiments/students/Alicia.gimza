# 2. Conception Assistée par Ordinateur (CAO)

Lors de cette session, nous avons été informés sur le fonctionnement de divers logiciels de modélisation 2D et 3D qui nous serviront par la suite du cours. Les logiciels présentés par le cours sont *[FreeCAD](https://www.freecad.org/)*, *[OpenSCAD](https://openscad.org/)* et *[Inkscape](https://inkscape.org/)*. Je documente dans ce module comment j'ai modélisé un objet afin de pouvoir l'imprimer à l'aide d'une des imprimantes 3D du FabLab. 

## Les images numériques

Il faut savoir qu'il existe deux types d'images numériques : les images *matricielles (ou pixelisées)* et les images *vectorielles*. *InkScape*, *FreeCAD* et *OpenSCAD* travaillent tous les 3 avec des images vectorielles. 

- D'où leur nom, les images *matricielles* sont composées d'un certain nombre de pixels : il s'agit surtout de dessins ou de photos numériques. Lorsque les pixels sont trop peu nombreux ou  trop grands, l'image baisse en qualité et en netteté : c'est ce qui arrive lorsque l'on veut aggrandir une image (zoom). Des logiciels comme [Adobe photoshop](https://www.adobe.com/be_en/products/photoshop/landpb.html?gclid=Cj0KCQjwlumhBhClARIsABO6p-xMxBM3MbmzVf269AN4i_qEuVnNoHjBALt4yGMVo_D20ZCLdt2XQmsaAkniEALw_wcB&mv=search&mv=search&sdid=LZ32SYVR&ef_id=Cj0KCQjwlumhBhClARIsABO6p-xMxBM3MbmzVf269AN4i_qEuVnNoHjBALt4yGMVo_D20ZCLdt2XQmsaAkniEALw_wcB:G:s&s_kwcid=AL!3085!3!445311000418!e!!g!!adobe%20photoshop!10432291863!103882922872&gad=1), [Microsoft paint](https://apps.microsoft.com/store/detail/paint/9PCFS5B6T72H), [GIMP](https://www.gimp.org/) ou [Adobe Lightroom](https://www.adobe.com/be_en/products/photoshop-lightroom.html?gclid=Cj0KCQjwlumhBhClARIsABO6p-zFSksMrQhuyuJBc4ipsA_qeCEp2kDTd7zsKBnGzDge6QnSaEDfN9gaApyzEALw_wcB&mv=search&s_kwcid=AL!3085!3!600825777730!e!!g!!adobe%20lightroom!1441876735!56642682659&mv=search&sdid=L7NVTQ8Y&ef_id=Cj0KCQjwlumhBhClARIsABO6p-zFSksMrQhuyuJBc4ipsA_qeCEp2kDTd7zsKBnGzDge6QnSaEDfN9gaApyzEALw_wcB:G:s&s_kwcid=AL!3085!3!600825777730!e!!g!!adobe%20lightroom!1441876735!56642682659&gad=1) utilisent principalement les images pixelisées. Concernant les formats, on retrouve souvent les images matricielles stockées dans des fichiers **.jpg** ou **.png**.


- Les images *vectorielles* en revanche, sont des images basées sur des équations mathématiques au lieu des pixels. Les images sont basées sur des formes géométriques (lignes, cercles, rectangles) et peuvent être agrandies ou rétrécies sans perdre de leur qualité. Elles sont souvent créées à l'aide d'un logiciel de conception graphique tel que [Adobe Illustrator](https://www.adobe.com/be_en/products/illustrator.html?gclid=Cj0KCQjwlumhBhClARIsABO6p-yMBibRZRwizejpJ8UHtvTMUhaS1bN21-F9xPvsPwZRQHweXmgCOWgaAliOEALw_wcB&mv=search&mv=search&sdid=KCJMVLF6&ef_id=Cj0KCQjwlumhBhClARIsABO6p-yMBibRZRwizejpJ8UHtvTMUhaS1bN21-F9xPvsPwZRQHweXmgCOWgaAliOEALw_wcB:G:s&s_kwcid=AL!3085!3!600520565898!e!!g!!adobe%20illustrator!1479062541!59972729489&gad=1) et sont utilisées généralement pour des dessins techniques ou des logos. Les images vectorielles ont généralement un format .ai, .eps, ou .pdf.


## Inkscape : la modélisation 2D
Inkscape est un outil de dessin vectoriel open-source . Il permet des modifications similaires aux applications qui utilisent également l'image vectorielle comme *Adobe Illustrator*. La haute résolution des images vectorielles les rendent idéales pour des impressions, mais elles sont également utilisées pour des modèles de découpes laser.



## FreeCAD et OpenSCAD : la modélisation 3D

Pour pouvoir modéliser un objet à imprimer en 3D, je vais devoir travailler avec des images vectorielles. Plusieurs logiciels sont utilisés pour travailler avec les imprimantes 3D dont [**FreeCAD**](https://www.freecad.org/) et [**OpenSCAD**](https://openscad.org/index.html).
Malgré que FreeCAD et OpenSCAD soient deux logiciens de modélisation 3D gratuits et open-source [^1], ils utilisent cependant deux approches différentes pour la conception et la modélisation :

- FreeCAD a une interface graphique assez intuitive et est basé sur des solides paramétriques : des formes géométriques de base (cubes, sphères, ...) sont utilisés pour créer des formes plus complexes en les combinant ou en les extrudant.

- OpenSCAD a une interface majoritairement textuelle et est basé sur des scripts : il faut écrire soi même du code pour créer les formes. Ces formes peuvent ensuite être translatées, rotationnées ou extruées. Ce logiciel est également beaucoup plus flexible.

Malgré que FreeCAD ait une interface graphique plutôt claire, j'ai décidé de travailler avec OpenSCAD car l'interface textuelle me paraissait plus intuitive. J'ai tout de même essayé FreeCAD, mais toutes les options différentes m'embrouillaient assez fort et faire une manipulation simple me prenait beaucoup trop de temps. Il existe une [cheat sheet](https://openscad.org/cheatsheet/) pour pouvoir utiliser OpenSCAD un peu plus aisément.

Une plateforme en ligne assez connue est [*Thingiverse*](https://www.thingiverse.com/), qui regroupe une quantité astronomique de modèles pour l'impression 3D. Ces modèles peuvent être téléchargés et modifiés par après. 


### Tentative #1

J'ai donc tenté de créer un objet un peu aléatoire en combinant plusieurs formes simples, le plus gros problème rencontré étant celui du manque d'inspiration et de créativité. Je me suis servie de la cheat sheet évoquée plus haut. Les photos suivantes illustrent le script utilisé et le résultat. 

![script](images/module2/SCADscript.png)

![résultat](images/module2/SCADmodele.png)


### Tentative #2 

J'ai également tenté d'imprimer une pièce qui serait emboitable avec celle de [martin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jean-francois.loumeau/). Je me suis renseignée sur les dimensions des embouts de ses pièces et j'ai tenté de garder les mêmes proportions. En plus de la structure qui s'emboite dans sa pièce, j'ai inséré une tige assez fine (ou du moins, ce que je ne pensais, mais je ne vais pas vous spoil) pour qu'elle soit flexible. 

J'ai utilisé le code suivant :

![mscfript](images/module2/modele2b.png)
![mscript](images/module2/modele2a.png)

Ce qui m'a donné comme résultat :

![resultat](images/module2/m2res.png)




## Creative Commons opens-source License

Peu importe le milieu dans lequel on se trouve lorsque l'on créé quelque chose (musique, art, documents,...) une license CC est primordiale. Elle permet à un auteur, si il le souhaite, de partager son travail qui peut-être utilisé par d'autres ou encore modifié tout en respectant les droits d'auteur. En somme, elle permet et encourage la collaboration et la créativité de pleins d'individus sur un interval de temps indéfini, pour autant que l'auteur de l'œuvre originale garde un contrôle sur les limites de comment son travail est partagé et utilisé. 

### Différents types de licenses 

Il existe plusieurs types de Licenses CC :

- **CC BY** (Attrbution)
- **CC BY-SA** (Attribution-ShareAlike)
- **CC BY-ND** (Attribution-NoDerivatives) 
- **CC BY-NC**  (Attribution-NonCommercial) 
- **CC BY-NC-SA** (Attribution-NonCommercial-ShareAlike) 
- **CC BY-NC-ND** (Attribution-NonCommercial-NoDerivatives)

Pour mieux comprendre la différence entre ces types de licenses, chaque duo de lettres a sa propre signification :

- **CC BY** (Attribution) : le travail peut être distribué, modifié et utilisé tant que les crédits sont attribués à l'auteur original
- **-SA** (ShareAlike) : si un travail basé sur l'œuvre originale est réalisé, il ne peut être sorti uniquement sous la *même license*
- **-ND** (NoDerivatives) : le travail ne peut pas être modifié ou adapté
- **-NC** (NonCommercial) : le travail ne peut pas être utilisé commercialement


### Choisir sa license

Pour ma documentation, je me réfère à [License Chooser](https://creativecommons.org/choose/#) pour trouver ce qui correspondrait le mieux à mon travail. J'ai choisi la license **CC-BY-SA** : je vais insérer le lien HTML généré sur ma page de présentation pour que l'icone soit facilement repérable. 

![cc](images/module2/CC.png)







[^1]