# 1. Gestion de projet et documentation

A travers le deuxième cours, j'ai découvert ce qu'était GitLab et comment j'allais documenter mon travail à travers ce site et plus précisément avec Git. La documentation permet de garder un historique assez précis du cheminement de notre travail ainsi que de notre évolution. N'ayant jamais codé auparavant, j'ai eu un peu du mal avec ces premières étapes : je n'ai donc pas pris le temps de tout annoter correctement.

Dès le premier cours, j'ai appris à utiliser le terminal ; c'est assez agréable je trouve, mais pas évident quand on a jamais dû l'utiliser de sa vie. Voici donc plusieurs commandes et conseils qui m'ont été fort utiles :

| Commande | Action |
|-----|-----------------|
| `cd` *(Change Directory)*| Permet de naviguer jusqu'à un fichier/dossier choisi |
| `ls` *(List)* | Aperçu des éléments présents dans ce fichier/dossier |
| `pwd` *(Print Working Directory)* | Détail du chemin suivi pour accéder au fichier/dossier |
| `cp` *(Copy)* | Copier |

Pour éviter de devoir tout ré-écrire, utiliser la flèche du haut (sur le clavier) retape les commandes qui ont été réalisées précédemment.
Pour naviguer vers un fichier ou directory depuis son bureau, il faut insérer chaque étape suivi d'un `/`, et les espaces précédés d'un `\`.
Par exemple, pour accéder à mon fichier *Alicia.gimza* localisé dans le dossier *PHYSF-517*, lui même localisé dans mon dossier *BA2 BIOL 2223* sur mon bureau, je vais utiliser la commande : 

`cd ~/Desktop/BA2\ BIOL\ 2223/PHYSF-517/Alicia.gimza`

Dans ce dossier , le fichier *Fabzero-modules* est localisé dans le dossier *docs*. Pour y accéder, je vais simplement rajouter le chemin additionnel :

`cd ~/Desktop/BA2\ BIOL\ 2223/PHYSF-517/Alicia.gimza/docs/Fabzero-modules`



## 1. Git

### 1.1. Télécharger Git

La première étape de la documentation consiste à télécharger et configurer GIT sur son ordinateur. Pour cela je me suis servie de la documentation trouvée sur [FabZero](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#git)


J'ai donc commencé par ouvrir Terminal et inséré  

`xcode-select --install`

je vérifie ensuite que Git fonctionne sur mon ordinateur avec 

`git --version`

Je téléchargé parallèlement Homebrew en suivant les indications [ici](https://brew.sh/index.html)

Dans le cas où je devrais mettre à jour la version de Git, j'ai tapé 

`brew update`

`brew update git`

j'ai ensuite vérifié que j'étais sur la bonne version avec

`git --version`



### 1.2. Configurer Git

Pour pouvoir annoter mon site correctement (ou n'importe quel autre projet), je dois entrer un identifiant afin de m'identifier comme l'auteure de mon travail. Pour cela je vais prendre les mêmes identifiants que j'utilise sur GitLab.
J'utilise donc

`git config --global user.name "alicia_gimza"`


`git config --global user.email "alicia.gimza@ulb.be"`

Je vérifie la configuration avec

`git config --global --list`

qui me donne les identifiants que je viens d'inscrire.

### 1.3. La clé SSH
Maintenant, je vais créer une connexion sécurisée entre mon ordinateur et le serveur GitLab.
Je commence par vérifier la version du SSH sur mon ordinateur avec 

`$ ssh -V`

la version doit être supérieure ou égale à 6.5.

Je créé ensuite une paire de clés : une publique et une privée. Ces clés vont me demander un passphrase (que je vais créer). 

`$ssh -keygen -t ed25519 -C "alicia.gimza@ulb.be"`

On me demande ensuite d'entrer un passphrase (qui est en réalité un mot de passe)
La clé publique SSH va être ajoutée à mon compte GitLab, pendant que je vais conserver la clé privée. Je copie donc la clé publique avec 

`$ cat ~/.ssh/id_ed25519.pub | pbcopy`
  
Dans le cas de l'utilisation d'un **windows**, il faudrait utiliser `clip` au lieu de pbcopy.



### 1.4 Cloner le projet 


Je peux maintenant cloner le site préalablement créée par le cours et le coller dans un repository sur mon ordinateur en faisant ces manipulations : 

![clonescreen](images/module1/clonescreen.png)

suivi de

`cd ~/Desktop/BA2\ BIOL\ 2223/PHYSF-517`
`$ git clone git@gitlab.com:fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/Alicia.gimza.git`

On me demande ensuite un passphrase : celui établi plus haut. 

Je peux à présent modifier et travailler sur mon site directement depuis mon ordinateur via la copie créée. La connexion sécurisée que j'ai crééé va me permettre de synchroniser ma copie avec le serveur GitLab. Je vais cependant avoir besoin de quelques commandes :



| Commande |  Description |
|-----|-----------------|
| git pull   | Télécharge la dernière version du projet |
| git add -A   | Ajoute toutes les modifications qui ont été faites | 
| git commit -m "message   | Commente les changements avec un message d'explication | 
| git push  | Envoie toutes les modifications au serveur |  


Après avoir fait l'erreur quelques fois, j'ai enfin retenu qu'il fallait bien faire attention à ne pas oublier d'ajouter toutes les modifications avant de commit.

![oups](images/oupsgit.png)


## 2. Documentation

La documentation étant primordiale pour ce cours, je vais devoir y consacrer beaucoup de temps et d'énergie : elle demande un travail assez rigoureux. Malgré cela et l'importance de la régularité, j'ai toujours eu énormément de mal à fournir un travail régulier ou quotidien et à réaliser des tâches avant la deadline imposée (étonnamment, ceci ne s'applique pas lorsqu'il s'agit de travaux de groupes). Tenir mon site à jour m'est donc pas évident, j'ai tendance à fonctionner beaucoup mieux pendant de longues heures intensives après un certain moment. Ma documentation n'est donc pas écrite immédiatement après les séances suivies, mais je prends cependant beaucoup de **notes** et de **photos** qui me permettent de développer le suivi des séances sur mon site en toute honnêteté.

Au niveau de la forme de ma documentation, j'ai décidé de rester assez franche sur ma façon d'expliquer et de communiquer. J'essaie de faire bien ressortir les points qui m'ont posés problème (souvent des erreurs de synthaxes ou simples points oubliés, comme par exemple utiliser des commandes pour windows alors que je suis sur mac), en espérant que les personnes qui suivront mon site dans le futur pourront comprendre les choses comme moi, biologiste n'ayant jamais eu de cours d'informatique, je les ai comprises. 

### 2.1. Editer son texte

Je vais écrire toute ma documentation en markdown pour ensuite l'exporter en HTML ; je travaille avec Visual Studio Code comme éditeur de texte. Avec Cmd+Shift+V, j'ai directement un aperçu de mon texte via une nouvelle page. J'ai utilisé [Markdown cheat sheet](https://www.markdownguide.org/cheat-sheet/) et [Markdown formidable outil](https://www.plpeeters.com/blog/fr/post73-le-markdown-un-formidable-outil) pour savoir comment procéder :

| Commande | Résultat | 
|----------|----------|
| `# Titre` | # Titre |
| `## Sous titre` | ## Sous titre |
| `### Sous sous titre` | ### Sous sous titre | IMAGES
| `**gras**`| **gras** |
| `*italique*`| *italique* |
| `==texte à surligner==` | ==Surligné== |
| ``code``| code|
| `![titre image](lienversl'image.jpg)`[^1]| Image |
| `[nom du lien] (adresse)`| Lien |
| `>citation` | Citation |
| `[x] (Lien correspondant vers x)`| X avec son lien respectif |
| `Texte à annoter[^1]`| Renvoyer un texte vers une note de bas de page |
| `[^1]: Note de bas de page` | Note de bas de page respective |
| ` 1. Premier élement ` \n ` 2. Deuxième élement ` | 1. Premier élement \n 2. Deuxième élement |
| ` - Element x ` \n ` - Element y ` | - Element x \n - Element y |
| `- [x] texte réalisé` \n `- [ ] texte à réaliser`| - [x] texte réalisé \n - [ ] texte à faire |




### 2.2. Convertir en HTML

La documentation écrite en markdown sont convertis en HTML via *MkDocs* qui peut être téléchargé [ici](https://www.mkdocs.org/getting-started/). 

Le projet du cours utilisé comporte ces fichiers :

- README.md
- requirements.txt
- .gitignore.txt
- mkdocs.yml *fichier de configuration*
- .gitlab-ci.yml
- docs *fichiers de documentation*
- FabZero-Modules
-- module01.md
-- module02.md
-- module03.md
-- module04.md
-- module05.md
-- module06.md
- images
- final-project.md
- index.md

Je vais apporter des modifications au fichier *mkdocs.yml*  pour pouvoir personnaliser ma page. `site_name` correspond au titre du site, `site_author` à son auteur.e, `site_url` à son URL et `repo_url` à son lien vers GitLab. J'ai apporté des modifications à ces 4 codes pour que l'on puisse identifier qu'il s'agit de ma documentation. 

![Modifications apportées](images/module1/mkdocsedit.png)

### 2.3. Images

Les images sont toujours assez utiles pour la documentation, j'apprends donc à les insérer sur mon projet. Cependant, afin de rester dans le cadre et le sujet du cours, je vais éditer mes photos afin de réduire leurs tailles pour éviter d'utiliser trop d'espace de stockage (pour rien). 
Les images en format JPEG vont directement perdre en qualité, car il s'agit d'un algorithme de compression : on optimise l'espace de stockage en limitant la taille. Je peux facilement convertir une image en PNG ou JPG : j'utilise de base Adobe Photoshop, où je peux surtout modifier mon image sous pas mal d'angles (rotations, luminosité, couleurs, montages,...) en plus de sa taille. 


Je peux faire ces opérations directement sur le control line interface grâce à *[GraphicsMagick](http://www.graphicsmagick.org/)*. Pour télécharger *GraphicsMagick*, je vais d'abord télécharger *[Homebrew*](https://brew.sh/)* qui va me permettre d'installer différents logiciels assez facilement. 

Pour télécharger *Homebrew*, j'utilise les commandes suivantes :
`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

et pour vérifier qu'il soit à jour :
`brew update`

pour télécharger *GraphicsMagick* :
`brew install graphicsmagick`

et je vérifie que l'installation soit réussie avec :
`gm version`

Il existe une grande variété de commandes possibles pour modifier des images avec *GraphicsMagick*. Avec l'aide d'internet, voici celles qui me paraissaient les plus importantes :

*Notez bien qu'avant tout ceci, il faut naviguer vers le directory où l'image se trouve avec la commande `cd`*


![resize](images/module1/gm.png)


- `gm convert -resize x% image.jpg nouvelleimage.jpg` : Diminue la taille de l'image de x% de ce qu'elle était à l'origine
- `gm convert -rotate 90 image.jpg nouvelleimage.jpg` : Pivote l'image originale de 90°


## 3. Issues et auto-évaluation

Pour mieux visualiser où on en est dans notre travail, un système **d'auto évaluation** disponible sur GitLab a été mis en place. Pour ce faire, il faut se rendre sur son projet personnelle du FabLab. 

### Importer les Issues

Les assignments n'ayant pas été insérés dans nos fichiers, il faut accéder aux templates mises en place et disponibles [ici](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/evaluation/#auto-evaluation-issues-for-module-4-and-5https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/evaluation/%23auto-evaluation-issues-for-module-4-and-5). De retour sur mon GitLab, je vais aller dans *Issues* et plus précisément dans *List*, où je vais importer les templates téléchargées en CSV.

![csv](images/module1/csv.png)


Dans la colonne de droite, je me *self assign* chaque évaluation. Je peux ensuite modifier l'état de mon avancement dans la case *Labels*. Les états d'avancement de chaque documentation se retrouve également dans *Boards*, ce qui donne une meilleure vue d'ensemble. Chaque couleur a sa propre signification : 


- Rouge : pas encore commencé
- Orange : en cours 
- Vert : terminé
- Bleu : terminé, reviewed par son binôme et corrigé par après.



![labels](images/module1/labels.png)
![board](images/module1/board.png)



[^1]: Il faut veiller à ce que l'image choisie soit bien dans le fichier du site sélectionné, et plus précisément dans **images** situé dans **fabzero-modules**