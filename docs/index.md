## Hello !

Je suis Alicia, plus connue sous le nom de "Grrimza" ou "la Gimz". J'aime la basse, le jazz et les avions. Je poursuis actuellement ma BA2 en Sciences Biologiques, ma 4e année à l'ULB. Jusqu'ici, j'ai vécu la moitié de ma vie à l'étranger suite à quoi j'ai fini par attérir ici, en Belgique. Je suis une personne très curieuse et j'aime toucher à tout, d'où mon choix pour ce cours à option où je me suis dit que mes idées et mon coté, je cite, "perché" pouvait être exploité dans l'intérêt d'un projet concret et réalisable. 

> *« Entre bigleuse et rageuse, y a pas énormément de différence chez toi » -Florian Brigode*

> *« Alicia tu sais, tu es le centre de mes soucis uniquement quand tu décides de l’être » -Arthur Kaye*

> *« Je suis fière de toi, Alichat » -Maman*

> *« Al, are you planning on tidying up your room ? » -Papa*



![coucou](images/8024resized.jpg)

*Me voici, en chair et en os, heureuse devant la scène alternative du bal des architectes*
## Background check

Dans le passé, je n'ai jamais réalisé de projets similaires à celui de ce cours. Cependant, j'ai eu l'occasion de créer la première de couverture d'un livre sorti à Muscat en 2008, et j'ai récemment gagné le prix de ma catégorie au [Concours photo du FFSB](https://www.ffsbxl.be/fr/pages/festival/concours.php) cette année. 




## La documentation
Ma documentation porte sur le cours [PHYS-F-517 "How to make (almost) any experiment"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/#list-of-fabzero-experiments-student-websites) qui a lieu au [FabLab](http://fablab-ulb.be/), et plus précisément sur chaque séance et formation que nous, étudiants, avons eu. 
Parmis les nombreuses choses que je voudrais faire plus tard, être professeure fait partie de mon top 3. J'ai une façon de communiquer (physique et orale) assez spontanée et honnête, où j'essaie de stimuler les autres et susciter l'intérêt. Vous l'auriez donc compris, ma documentation n'est pas loin de mes intéractions réelles ! La façon de formuler mes phrases est dans l'optique de non seulement vous faire *comprendre* ce cours comme j'ai pu le voir, mais aussi de rendre la lecture plus *entrainante et agréable* : ça me semble surtout crucial lorsqu'il y a des tâches plus complexes ou fatiguantes à réaliser. Il s'agit ici de *ma* documentation, alors pourquoi rédiger un site similaire à un manuel impersonnel ?



<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.